import React from 'react'
import { render } from 'react-dom'
import { App } from './components/App'
import { NotFound } from './components/NotFound'
import { Router, Route, hashHistory } from 'react-router'
import './stylesheets/ui.scss'
import './stylesheets/index.scss'

window.React = React

render(
  <Router history={hashHistory}>
    <Route path="/" component={App}/>
    <Route path="list-days" component={App}>
      <Route path=":filter" component={App} />
    </Route>
    <Route path="add-day" component={App} />
    <Route path="*" component={NotFound}/>
  </Router>,
  document.getElementById('react-container')
)
